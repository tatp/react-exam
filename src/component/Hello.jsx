import React, { useState } from 'react'

const Hello = () => {
  const [inputName, setInputName] = useState('')
  const [name, setName] = useState('')

  const showName = () => {
    setName(inputName)
  }

  return (
    <div>
      <div>Enter your name: {name}</div>
      <input type='text' value={inputName} onChange={(event) => {setInputName(event.target.value)}}/>
      <button onClick={showName}>Submit</button>
    </div>
  )
}

export default Hello