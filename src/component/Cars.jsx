import React from 'react'

const cars = ['Ford', 'BMW', 'Audi']

const Cars = () => {
  return (
    <div>
      <h1>Who lives in my garage?</h1>
      <ul>
        {
          cars.map(car => (
            <li>{`I am a ${car}`}</li>
          ))
        }
      </ul>
    </div>
    
  )
}

export default Cars