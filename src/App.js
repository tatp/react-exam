import logo from './logo.svg';
import './App.css';
import Hello from './component/Hello';
import Cars from './component/Cars';

function App() {
  return (
    <div>
      <Hello />
      <Cars />
    </div>
  );
}

export default App;
